import 'package:flutter/material.dart';
import 'product_detail_view.dart';
import '../Model/product_mold.dart';
import 'dart:convert';

class product_list_view extends StatelessWidget {
  const product_list_view({Key? key}) : super(key: key);

  Future<String> cargarArchivo(BuildContext context) async {
    var result = await DefaultAssetBundle.of(context)
        .loadString('assets/json/products.json');
    return result;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Productos'),
          titleTextStyle: const TextStyle(color: Colors.white, fontSize: 20),
          iconTheme: const IconThemeData(color: Colors.white),
          backgroundColor: Colors.purple,
        ),
        body: Container(
          child: Center(
            child: FutureBuilder(
              future: cargarArchivo(context),
              builder: (context, AsyncSnapshot<String> snapshot) {
                if (snapshot.hasData) {
                  Map<String, dynamic> data = json.decode(snapshot.data!);
                  var products = Products.fromJson(data);
                  return ListView.builder(
                    itemCount: products.products.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProductDetailsView(
                                product: products.products[index],
                              ),
                            ),
                          );
                        },
                        child: Hero(
                          tag:
                              "product_${products.products[index].description}",
                          child: Card(
                            child: ListTile(
                              title: Text(products.products[index].name),
                              subtitle:
                                  Text("${products.products[index].price}"),
                              leading: Image.asset(
                                products.products[index].imageUrl,
                                width: 40,
                                height: 40,
                              ),
                              trailing: const Icon(Icons.arrow_forward),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ),
      );
}
