class Products {
  late List<Product> products;

  Products({required this.products});

  Products.fromJson(Map<String, dynamic> json) {
    if (json['products'] != null) {
      products = <Product>[];
      json['products'].forEach((v) {
        products.add(Product.fromJson(v));
      });
    }
  }
}

class Product {
  late String name;
  late double price;
  late String imageUrl;
  late String description;

  Product({
    required this.name,
    required this.price,
    required this.imageUrl,
    required this.description,
  });

  Product.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    price = json['price'];
    imageUrl = json['image'];
    description = json['description'];
  }
}
